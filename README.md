This tool provides a means to add keywords (mainly similar to RCS) expansion to your git projects.
This is derived from [git-rcs-keywords by Martin Turon](https://github.com/turon/git-rcs-keywords).

## Keywords used

The keywords are not strictly similar to RCS, they are: _$All$, $Date$, $File$,$Author$, $Revision$, $Source$, $Version$_ !

Note : 

1. there's no _$Id$_ tag : the equivalent is _$All$_.
The _$Id$_ tag is already used by the ident filter as described in the [git documentation]
(https://git-scm.com/book/en/v2/Customizing-Git-Git-Attributes#_keyword_expansion)

2. the _$Version$_ keyword will be replaced by the tag used on the commit ... if any.
 
## Installation
 
The mechanism used are filters as described in [git documentation](https://git-scm.com/book/en/v2/Customizing-Git-Git-Attributes#_keyword_expansion).
The **smudge filter** is run on _checkout_, and the **clean filter** is run on _commit_.
The keywords are only expanded on the local disk, not in the repository itself.

To start, you need to add the following lines to _~/.gitconfig_ file:

    [filter "mc-keywords"]
    	clean  = .git_filters/mc-keywords.clean
    	smudge = .git_filters/mc-keywords.smudge %f

Then, to add keyword expansion, simply copy these 3 files to your project:

    <project>/.gitattributes
    <project>/.git_filters/mc-keywords.smudge
    <project>/.git_filters/mc-keywords.clean

The given _.gitattributes_ is for PHP projects but will work for others as long as you change the extension according to your needs.

**Note** : keyword expansion is **in most cases not recommanded** and I am not using it eventually.
In particular, this keyword expansion will not work on export (i.e. generated tarball).